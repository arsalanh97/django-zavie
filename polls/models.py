from django.db import models

# Create your models here.
class Question (models.Model):
    qtext = models.CharField(max_length=200)
    qdate = models.DateTimeField('date published')

    def __str__(self):
        return self.qtext


class Choise (models.Model):
    question = models.ForeignKey(Question , on_delete=models.CASCADE)
    ctext = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.ctext